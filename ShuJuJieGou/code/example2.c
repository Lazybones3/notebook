#include<stdio.h>

void PrintN1(int N) 
{
    for (int i = 0; i <= N; i++)
    {
        printf("%d\n", i);
    }
}

void PrintN2(int N)
{
    if (N)
    {
        PrintN2(N - 1);
        printf("%d\n", N);
    }
    
}

int main(int argc, char const *argv[])
{
    int N;
    scanf("%d", &N);
    PrintN1(N);
    //PrintN2(N);
    return 0;
}
