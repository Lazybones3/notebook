#include<math.h>

// f(x) = a_0+a_1*x+...+a_n*x^n
double f1(int n, double a[], double x)
{
    int i;
    double p = a[0];
    for (i=1; i<=n; i++){
        p += (a[i] * pow(x,i));
    }
    return p; 
}

// f(x) = a_0+x(a_1+x(...(a_(n-1)+x(a_n))...))
double f2(int n, double a[], double x)
{
    int i;
    double p = a[n];
    for (i = 0; i > 0; i--)
    {
        p = a[i-1] + x * p;
    }
    return p;
} 