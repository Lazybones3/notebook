#include<stdio.h>
#include<time.h>

clock_t start, stop;
double duration;
#define MAXN 10 /* 多项式最大项数 */
#define MAXK 1e7

double f1(int n, double a[], double x);
double f2(int n, double a[], double x);

int main(int argc, char const *argv[])
{
    int i;
    double a[MAXN];
    for (i = 0; i < MAXN; i++)
    {
        a[i] = (double)i;
    }

    //clock()：捕捉从程序开始运行到clock()被调用时所耗费的时间。
    //这个时间单位是clock tick，即“时钟打点”。
    //常数CLK_TCK(或CLOCKS_PER_SEC)：机器时钟每秒所走的时钟打点数。
    start = clock();
    for (i = 0; i < MAXK; i++)
        f1(MAXN-1, a, 1.1);
    stop = clock();
    duration = ((double)(stop - start))/CLOCKS_PER_SEC/MAXK;
    printf("ticks1 = %f\n", (double)(stop - start));
    printf("duration1 = %6.2e\n", duration);

    start = clock();
    for (i = 0; i < MAXK; i++)
        f2(MAXN-1, a, 1.1);
    stop = clock();
    duration = ((double)(stop - start))/CLOCKS_PER_SEC/MAXK;
    printf("ticks2 = %f\n", (double)(stop - start));
    printf("duration2 = %6.2e\n", duration);
    return 0;
}
