1. 安装编译工具[protoc](https://github.com/protocolbuffers/protobuf/releases)，下载protoc-3.14.0-osx-x86_64.zip，解压后将protoc添加环境变量；

2. 安装编译插件[protoc-gen-grpc-java](https://mvnrepository.com/artifact/io.grpc/protoc-gen-grpc-java)，点击View All下载protoc-gen-grpc-java-1.33.1-osx-x86_64.exe

3. 编写helloworld.proto文件，运行以下命令进行编译，--proto_path指定proto文件的存储路径，--java_out和--grpc-java_out指定文件输出路径：

   ```
   protoc --java_out=src/main/java --proto_path=src/main/proto helloworld.proto
   protoc --plugin=protoc-gen-grpc-java=protoc-gen-grpc-java-1.33.1-osx-x86_64.exe --grpc-java_out=src/main/java --proto_path=src/main/proto helloworld.proto
   ```

4. 编写测试代码：

   Helloworld.proto

   ```protobuf
   syntax = "proto3";
   
   option java_multiple_files = true;
   option java_package = "io.grpc.examples.helloworld";
   option java_outer_classname = "HelloWorldProto";
   option objc_class_prefix = "HLW";
   
   package helloworld;
   
   service Greeter {
     rpc SayHello (HelloRequest) returns (HelloReply) {}
   }
   
   message HelloRequest {
     string name = 1;
   }
   
   message HelloReply {
     string message = 1;
   }
   ```

   HelloWorldServer.java

   ```java
   import io.grpc.Server;
   import io.grpc.ServerBuilder;
   import io.grpc.examples.helloworld.GreeterGrpc;
   import io.grpc.examples.helloworld.HelloReply;
   import io.grpc.examples.helloworld.HelloRequest;
   import io.grpc.stub.StreamObserver;
   
   import java.io.IOException;
   import java.util.concurrent.TimeUnit;
   
   public class HelloWorldServer {
   
       private Server server;
   
       private void start() throws IOException, InterruptedException {
           int port = 50051;
           server = ServerBuilder.forPort(port)
                   .addService(new GreeterImpl())
                   .build()
                   .start();
           System.out.println("Server started...");
           Runtime.getRuntime().addShutdownHook(new Thread() {
               @Override
               public void run() {
                   System.err.println("*** shutting down gRPC server since JVM is shutting down");
                   try {
                       HelloWorldServer.this.stop();
                   } catch (InterruptedException e) {
                       e.printStackTrace(System.err);
                   }
                   System.err.println("*** server shut down");
               }
           });
       }
   
       private void stop() throws InterruptedException {
           if (server != null) {
               server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
           }
       }
   
       private void blockUntilShutdown() throws InterruptedException {
           if (server != null) {
               server.awaitTermination();
           }
       }
   
       public static void main(String[] args) throws IOException, InterruptedException {
           final HelloWorldServer server = new HelloWorldServer();
           server.start();
           server.blockUntilShutdown();
       }
   
       static class GreeterImpl extends GreeterGrpc.GreeterImplBase {
           @Override
           public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
               System.out.println("receive:"+request.getName());
               HelloReply reply = HelloReply.newBuilder().setMessage("Hello, " + request.getName()).build();
               responseObserver.onNext(reply);
               responseObserver.onCompleted();
           }
       }
   }
   ```

   HelloWorldClient.java

   ```java
   import io.grpc.Channel;
   import io.grpc.ManagedChannel;
   import io.grpc.ManagedChannelBuilder;
   import io.grpc.StatusRuntimeException;
   import io.grpc.examples.helloworld.GreeterGrpc;
   import io.grpc.examples.helloworld.HelloReply;
   import io.grpc.examples.helloworld.HelloRequest;
   
   public class HelloWorldClient {
   
       private final GreeterGrpc.GreeterBlockingStub blockingStub;
   
       public HelloWorldClient(Channel channel) {
           this.blockingStub = GreeterGrpc.newBlockingStub(channel);
       }
   
       public void greet(String name) {
           System.out.println("Will try to greet " + name + " ...");
           HelloRequest request = HelloRequest.newBuilder().setName(name).build();
           HelloReply response;
           try {
               response = blockingStub.sayHello(request);
           } catch (StatusRuntimeException e) {
               e.printStackTrace();
               return;
           }
           System.out.println("Greeting: " + response.getMessage());
       }
   
       public static void main(String[] args) {
           String user = "world";
           String target = "localhost:50051";
   
           ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
                   .usePlaintext()
                   .build();
   
           HelloWorldClient client = new HelloWorldClient(channel);
   
           client.greet(user);
       }
   }
   ```

   

